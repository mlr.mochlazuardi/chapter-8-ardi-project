const express = require('express')
const app = express()
const cors = require('cors')
const apiRouter = require('./server/routes')

const cookieParser = require('cookie-parser');
const session = require('express-session');
const flash = require('express-flash');
const passport = require("./server/libs/passport");
const passportJwt = require("./server/libs/passport-jwt");
const errorHandler = require('./server/middlewares/errorHandler')
const PORT = process.env.PORT || 4000

// middlewares
app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(errorHandler)

app.use(cookieParser());
app.use(flash());
app.use(session({
  secret: 'secretkey',
  resave: false,
  saveUninitialized: false
}));

// LocalStrategy
app.use(passport.initialize());
app.use(passport.session());  

// JWT
app.use(passportJwt.initialize());

/**
 * @Routes /api
 * entrypoint for all API routes
 */
app.set('view engine', 'ejs');
app.use("/api", apiRouter);
app.set('views', './server/views');

app.listen(PORT, () => {
  console.log(`Listening on http://localhost:${PORT}`)
})
