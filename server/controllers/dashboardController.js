const { Player, Room } = require("./../models");

module.exports = {
    index : async (req, res, next) => {

        // Count Data User
        const countUser = await Player.count();
        // const countRoom = await Room.count();
        // const countUser = 12;
        const countRoom = 3;

        res.render(
            "admin", 
            { 
                page: { title: "Halaman admin!"}, 
                user: req.user,
                content : [
                    { title : "PLAYER", count: countUser, grid : 6  },
                    { title : "ROOM", count: countRoom, grid: 6  }
                  ]
            });
    }
}