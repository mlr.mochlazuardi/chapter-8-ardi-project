const checkRole = (role) => {
    return function (req, res, next) {
        if (req.isAuthenticated() && role.includes(req.user.role_id.name)) {
            return next();
        }
        res.redirect('/api/login');
    }
}

module.exports = checkRole;